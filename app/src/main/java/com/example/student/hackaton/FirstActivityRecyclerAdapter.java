package com.example.student.hackaton;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FirstActivityRecyclerAdapter extends RecyclerView.Adapter<FirstActivityRecyclerAdapter.ViewHolder> {

    private List<Money> moneyList = new ArrayList<>();

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.image)
        ImageView imageView;

        @BindView(R.id.name)
        TextView nameTextView;

        @BindView(R.id.name_full)
        TextView nameFullTextView;


        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public FirstActivityRecyclerAdapter(List<Money> moneyList) {
        this.moneyList = moneyList;
    }

    @NonNull
    @Override
    public FirstActivityRecyclerAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.first_activity_listitem, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull FirstActivityRecyclerAdapter.ViewHolder holder, int position) {
        holder.imageView.setImageResource(getMoney(position).getImage());
        holder.nameTextView.setText(getMoney(position).getName());
        holder.nameFullTextView.setText(getMoney(position).getFullName());
    }

    @Override
    public int getItemCount() {
        return moneyList.size();
    }

    private Money getMoney(int position) {

        return moneyList.get(position);
    }
}
