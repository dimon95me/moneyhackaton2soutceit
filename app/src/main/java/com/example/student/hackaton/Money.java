package com.example.student.hackaton;

public class Money {
    private int image;
    private String name;
    private String fullName;
    private String info;

    public Money(int image, String name, String fullName, String info) {
        this.image = image;
        this.name = name;
        this.fullName = fullName;
        this.info = info;

    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }
}
