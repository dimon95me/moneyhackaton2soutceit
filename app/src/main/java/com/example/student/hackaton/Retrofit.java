package com.example.student.hackaton;

import retrofit.RestAdapter;
import retrofit.http.GET;

public class Retrofit {

    private static final String ENDPOINT = "http://data.fixer.io/api/";
    private static final String ACCESS_KEY = "f8d8ead9582006db022d0149e82aabbc";
    private static ApiInterface apiInterface;

    static {
        initialize();
    }

    interface ApiInterface {
        @GET("latest")
        void getCountries(Callback<List<Country>> callback);
    }

    public static void initialize() {
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(ENDPOINT)
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .build();
        apiInterface = restAdapter.create(ApiInterface.class);
    }

    public static void getCountries(Callback<Money> callback) {
        apiInterface.getCountries(callback);
    }

    
}
