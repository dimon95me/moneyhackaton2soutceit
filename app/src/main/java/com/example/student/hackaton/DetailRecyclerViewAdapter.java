package com.example.student.hackaton;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DetailRecyclerViewAdapter extends RecyclerView.Adapter<DetailRecyclerViewAdapter.ViewHolder> {
    List<Money> moneyList = new ArrayList<>();

    public DetailRecyclerViewAdapter(List<Money> moneyList) {
        this.moneyList = moneyList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.detail_listitem, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.image.setImageResource(moneyList.get(position).getImage());
        holder.name.setText(moneyList.get(position).getName());
        holder.nameFull.setText(moneyList.get(position).getFullName());
        holder.info.setText(moneyList.get(position).getInfo());
    }

    @Override
    public int getItemCount() {
        return moneyList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.detail_image)
        ImageView image;

        @BindView(R.id.detail_name)
        TextView name;

        @BindView(R.id.detail_name_full)
        TextView nameFull;

        @BindView(R.id.detail_info)
        TextView info;

        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}
