package com.example.student.hackaton;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindInt;
import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.recycler_view)
    RecyclerView recycler;

    List<Money> moneyList = new ArrayList<>();
    FirstActivityRecyclerAdapter firstActivityRecyclerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        firstActivityRecyclerAdapter = new FirstActivityRecyclerAdapter(moneyList);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        recycler.setLayoutManager(layoutManager);
        recycler.setItemAnimator(new DefaultItemAnimator());
        recycler.setAdapter(firstActivityRecyclerAdapter);

        createData();
    }

    private void createData(){

        moneyList.add(new Money(R.drawable.usd_dollar_money_cash, "USD", "United States Dollar", "gnodghosd"));
        moneyList.add(new Money(R.drawable.euro, "EUR", "Euro", "gnodghosd"));
        moneyList.add(new Money(R.drawable.rubl, "RUR", "Ticket of russian bank", "gnodghosd"));

        firstActivityRecyclerAdapter.notifyDataSetChanged();
    }
}
